# Welcome to Qlue

For signup visit [hfak-fermions.herokuapp.com](https://hfak-fermions.herokuapp.com/).

# Qlue : a web framework for quantum computing backends
--------
* [General description](guides/gen_guide.md) : Details of the infrastructure.
* [User Guide](guides/user_guide.md) : For users who want to use our service.
* [Developer Guide](guides/dev_guide.md) : For people who want to implement this on their own for their research.


## Our projects:
--------
* [``qlue``](https://gitlab.mpcdf.mpg.de/mpq-unirand/hfak-project/qlue) - API code.
* [``pennylane-ls``](https://gitlab.mpcdf.mpg.de/mpq-unirand/hfak-project/labscript-ls) - Pennylane examples.
* [``qiskit-cold-atom``](https://github.com/Qiskit-Extensions/qiskit-cold-atom) - QisKit examples.
