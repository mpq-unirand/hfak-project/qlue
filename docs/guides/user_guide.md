# User Guide
To use our existing service, the steps are very simple:

* Install either [Qiskit][Qiskit_github] or [Pennylane][Pennylane_github].
* Make yourself familiar with how to write quantum circuits in these frameworks.
* Signup at [hfak-fermions.herokuapp.com](https://hfak-fermions.herokuapp.com/) for an account. At the moment the signup experience is very bad e.g. no confirmation shows up after you signup, no password reset feature is available etc. We are upgrading things and it will become more professional. But the username and password you choose during signup will work.
* Look at our examples in which we explain circuit implementation of some previous experimental results achieved with cold atoms at Uni-Heidelberg.
    * QisKit examples : [``qiskit-cold-atom``](https://github.com/Qiskit-Extensions/qiskit-cold-atom)
    * Pennylane examples : [``pennylane-ls``](https://gitlab.mpcdf.mpg.de/mpq-unirand/hfak-project/labscript-ls)
* Start submitting your jobs. For the moment we only provide simulator backends.


[Qiskit_github]: https://github.com/Qiskit "Qiskit"
[Pennylane_github]: https://github.com/PennyLaneAI "Pennylane"